import { styled } from '@mui/material'
import { TextFieldStatus } from "../../constants/input"

export const OTPSingleInput = styled('input', { shouldForwardProp: (prop) => prop !== 'state' })<{
  state: TextFieldStatus
}>(({ theme, state }) => {
  return {
    width: '47px',
    height: '56px',
    backgroundColor: theme.palette['secondary.1'][theme.palette.mode],
    borderRadius: '4px',
    color: '#000',
    paddingLeft: '8px',
    paddingRight: '8px',
    textAlign: 'center',
    border: `${
      state === 'default'
        ? 'none'
        : `1px solid ${
            state === 'error'
              ? theme.palette['red.1'][theme.palette.mode]
              : theme.palette['primary.1'][theme.palette.mode]
          }`
    }`,
    ':focus': {
      outline: 'none',
    },
  }
})