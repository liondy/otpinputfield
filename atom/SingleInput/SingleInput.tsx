import React, { memo, useRef, useLayoutEffect } from 'react'
import usePrevious from '../../utils/hooks/usePrevious'
import { TextFieldStatus } from '../../constants/input'
import { OTPSingleInput } from './element'

export interface SingleOTPInputProps extends React.InputHTMLAttributes<HTMLInputElement> {
  focus?: boolean
  state: TextFieldStatus
}

export function SingleOTPInputComponent({ focus, state, ...rest }: SingleOTPInputProps) {
  const inputRef = useRef<HTMLInputElement>(null)
  const prevFocus = usePrevious(!!focus)
  useLayoutEffect(() => {
    if (inputRef.current) {
      if (focus) {
        inputRef.current.focus()
      }
      if (focus && focus !== prevFocus) {
        inputRef.current.focus()
        inputRef.current.select()
      }
    }
  }, [focus, prevFocus])
  return <OTPSingleInput state={state} ref={inputRef} {...rest} />
}

const SingleOTPInput = memo(SingleOTPInputComponent)
export default SingleOTPInput
