import React, { memo, useState, useCallback, useEffect } from 'react'
import { TextFieldStatus } from '../../constants/input'
import SingleInput from '../../atom/SingleInput/SingleInput'

export interface OTPInputProps {
  onChangeOTP: (otp: string) => any
  state: TextFieldStatus
}

export function OTPInputComponent({ onChangeOTP, state }: OTPInputProps) {
  const length = 6

  const [activeInput, setActiveInput] = useState(0)
  const [otpValues, setOTPValues] = useState(Array<string>(length).fill(''))
  const [status, setStatus] = useState<TextFieldStatus>(state)

  // Helper to return OTP from inputs
  const handleOtpChange = useCallback(
    (otp: string[]) => {
      const otpValue = otp.join('')
      onChangeOTP(otpValue)
    },
    [onChangeOTP]
  )

  // Helper to return value with the right type: 'text' or 'number'
  const getRightValue = useCallback((str: string) => {
    let changedValue = str
    if (!changedValue) {
      return ''
    }
    return Number(changedValue) >= 0 ? changedValue : ''
  }, [])

  // Change OTP value at focussing input
  const changeCodeAtFocus = useCallback(
    (str: string, back: number = 0) => {
      if (activeInput + back >= 0) {
        const updatedOTPValues = [...otpValues]
        updatedOTPValues[activeInput + back] = str[0] || ''
        setOTPValues(updatedOTPValues)
        handleOtpChange(updatedOTPValues)
      }
    },
    [activeInput, handleOtpChange, otpValues]
  )

  // Focus `inputIndex` input
  const focusInput = useCallback(
    (inputIndex: number) => {
      const selectedIndex = Math.max(Math.min(length - 1, inputIndex), 0)
      setActiveInput(selectedIndex)
    },
    [length]
  )

  const focusPrevInput = useCallback(() => {
    focusInput(activeInput - 1)
  }, [activeInput, focusInput])

  const focusNextInput = useCallback(() => {
    focusInput(activeInput + 1)
  }, [activeInput, focusInput])

  // Handle onFocus input
  const handleOnFocus = useCallback(
    (index: number) => () => {
      focusInput(index)
    },
    [focusInput]
  )

  // Handle onChange value for each input
  const handleOnChange = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      const val = getRightValue(e.currentTarget.value)
      if (!val) {
        e.preventDefault()
        changeCodeAtFocus(val)
        setActiveInput(-1)
        return
      }
      changeCodeAtFocus(val)
      focusNextInput()
    },
    [changeCodeAtFocus, focusNextInput, getRightValue]
  )

  // Handle onBlur input
  const onBlur = useCallback(() => {
    setActiveInput(-1)
  }, [])

  // Handle onKeyDown input
  const handleOnKeyDown = useCallback(
    (e: React.KeyboardEvent<HTMLInputElement>) => {
      const pressedKey = e.key
      switch (pressedKey) {
        case 'Backspace':
        case 'Delete': {
          e.preventDefault()
          if (otpValues[activeInput]) {
            changeCodeAtFocus('')
          } else {
            focusPrevInput()
            changeCodeAtFocus('', -1)
          }
          break
        }
        case 'ArrowLeft': {
          e.preventDefault()
          focusPrevInput()
          break
        }
        case 'ArrowRight': {
          e.preventDefault()
          focusNextInput()
          break
        }
        default: {
          if (pressedKey.match(/^[^a-zA-Z0-9]$/)) {
            e.preventDefault()
          }
          break
        }
      }
    },
    [activeInput, changeCodeAtFocus, focusNextInput, focusPrevInput, otpValues]
  )

  const handleOnPaste = useCallback(
    (e: React.ClipboardEvent<HTMLInputElement>) => {
      e.preventDefault()
      const pastedData = e.clipboardData
        .getData('text/plain')
        .trim()
        .slice(0, length - activeInput)
        .split('')
      if (pastedData) {
        let nextFocusIndex = 0
        const updatedOTPValues = [...otpValues]
        updatedOTPValues.forEach((val, index) => {
          if (index >= activeInput) {
            const changedValue = getRightValue(pastedData.shift() || val)
            if (changedValue) {
              updatedOTPValues[index] = changedValue
              nextFocusIndex = index
            }
          }
        })
        setOTPValues(updatedOTPValues)
        setActiveInput(Math.min(nextFocusIndex + 1, length - 1))
        const otpValue = updatedOTPValues.join('')
        onChangeOTP(otpValue)
      }
    },
    [activeInput, getRightValue, length, otpValues, onChangeOTP]
  )

  useEffect(() => {
    if (otpValues.every((e) => e === otpValues[0])) {
      setStatus('default')
    }
  }, [otpValues])

  useEffect(() => {
    let timeoutReset: any = ''
    setStatus(state)
    if (state === 'error') {
      timeoutReset = setTimeout(() => {
        const updateOTPValues = new Array(length).fill('')
        setOTPValues(updateOTPValues)
        setActiveInput(0)
        handleOtpChange(updateOTPValues)
      }, 500)
    }
    return () => {
      clearTimeout(timeoutReset)
    }
  }, [state, onBlur, handleOtpChange])

  return (
    <div style={{ display: 'flex' }}>
      {Array(length)
        .fill('')
        .map((_, index) => (
          <SingleInput
            key={`SingleInput-${index}`}
            focus={activeInput === index}
            value={otpValues && otpValues[index]}
            onFocus={handleOnFocus(index)}
            onChange={handleOnChange}
            onKeyDown={handleOnKeyDown}
            onBlur={onBlur}
            onPaste={handleOnPaste}
            state={status}
            style={{ marginRight: '11px' }}
          />
        ))}
    </div>
  )
}

const OTPInput = memo(OTPInputComponent)
export default OTPInput